function f3 = objfunc_3(x)   
% objective function (minimization)
f3 =(((1.0/4000.0)*(x(:,1).^2+x(:,2).^2+x(:,3).^2+x(:,4).^2+x(:,5).^2))...
    -cos(x(:,1)/sqrt(1)).*cos(x(:,2)/sqrt(2)).*cos(x(:,3)/sqrt(3)).*cos(x(:,4)/sqrt(4)).*cos(x(:,5)/sqrt(5))+1);
end