function f1 = objfunc_1(x)   
% objective function (minimization)
f1 = (4-2.1*x(1).^2 +(1.0/3.0)*x(1).^4).*x(1).^2 +x(1).*x(2)+ (-4+4*x(2).^2).*x(2).^2;
end

