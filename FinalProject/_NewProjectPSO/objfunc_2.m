function f2 = objfunc_2(x)   
% objective function (minimization)
aVal=20;bVal=0.2;cVal=2*pi;
f2 = (-aVal*exp(-bVal*sqrt((1.0/5.0)*(x(:,1).^2+x(:,2).^2+x(:,3).^2+x(:,4).^2+x(:,5).^2)))...
    -exp((1.0/5.0)*(cos(cVal*x(:,1))+cos(cVal*x(:,2))+cos(cVal*x(:,3))+cos(cVal*x(:,4))+cos(cVal*x(:,5))))+aVal+exp(1));
% set the position of the initial swarm
end

