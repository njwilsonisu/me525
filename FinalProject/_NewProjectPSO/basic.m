function [x,fval,exitflag,output] = basic(fun,nvars,lb,ub,SwarmSize,FunctionTolerance,MaxIterations,SelfAdjustmentWeight,SocialAdjustmentWeight,UseParallel,UseVectorized)

rng default;

% Upper and Lower Bounds
%lb = [0 0];
%ub = [10 10];

%Size of the Swarm
%SwarmSize = 20;

%Maximum Number of Iterations
%MaxIterations = 100;

% c1
%SelfAdjustmentWeight = 1.49;
% c2
%SocialAdjustmentWeight = 1.49;

% Range of values for W
InertiaRange = [1,1];

%Neighborhood Fraction
MinNeighborsFraction = 0;


options = optimoptions( ...
    'particleswarm', ...
    'SwarmSize',SwarmSize, ...
    'FunctionTolerance',FunctionTolerance, ...
    'MaxIterations', MaxIterations, ...
    'SelfAdjustmentWeight', SelfAdjustmentWeight, ...
    'SocialAdjustmentWeight', SocialAdjustmentWeight, ...
    'InertiaRange', InertiaRange, ...
    'UseParallel', UseParallel, ...
    'UseVectorized', UseVectorized, ...
    'MinNeighborsFraction', MinNeighborsFraction, ...
    'Display', 'off' ...
    );
    %'PlotFcn', 'pswplotbestf', ...

[x,fval,exitflag,output] = particleswarm(fun,nvars,lb,ub,options);

% fprintf('\n Optimum: [%0.2f,%0.2f] \n F value: %0.2f \n \n ',x(1),x(2),fval);


end