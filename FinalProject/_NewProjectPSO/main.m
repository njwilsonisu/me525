clear;
clc;
rng default;

% These are the names of the functions that we will run, each of them runs
% their respective pso method with the parameter that we will define
Methods = {'basic' 'inertiaWeight' 'constriction' 'neighborhood'};

for methodNum = 1:numel(Methods)
    
    method = string(Methods(methodNum));
    m = str2func(method);
    
    % Setup the Parameters
    
    % For some of the PSO methods, Some of these parameters will be overwritten
    % because the method handles their values
    
    % ONLY USED BY BASIC AND INERTIA WEIGHT
    if strcmp(method, "basic") || strcmp(method, "inertiaWeight")
        % c1
        SelfAdjustmentWeight = [ 1.2 ];
        % c2
        SocialAdjustmentWeight = [ 1.2 ];
    else
        % If the method is not basic, the values will be handled
        SelfAdjustmentWeight = [ 1.495 ];
        SocialAdjustmentWeight = [ 1.495 ];
    end
    
    % Number of times we run each case
    TestRuns = 100;
    
    % Objective Function
    objfunction = @actual_objfunc;
    
    % Number of Variables
    nVars = 2;
    
    % Upper and Lower Bounds
    lb = [0 0];
    ub = [10 10];
    
    %Size of the Swarm
    SwarmSize = [5 10 20 50];
    
    % Convergence Tolerance
    FunctionTolerance = 1e-9;
    
    %Maximum Number of Iterations
    MaxIterations = 500;
    
    UseParallel = false;
    
    UseVectorized = false;
    
    %% Intialize Data Storage Matrix
    
    % Number of Cases
    n1 = numel(SwarmSize);
    n2 = numel(SelfAdjustmentWeight);
    n3 = numel(SocialAdjustmentWeight);
    
    % Each Row represents a run case with the following column setup
    labels = {'Swarm Size','Successful Runs','Avg F','Avg Iterations','Avg Function Calls','Total Time','Optimum Qc','Optimum Qh','Minimum F'};
    data = [n1*n2*n3 , 9];
    
    %% Run Loop
    
    for i = 1:n1
        for j = 1:n2
            for k = 1:n3
                
                % ------- New Test Case -------
                
                % Number of Successful Runs for this Test Case
                SuccessfulRuns = 0;
                
                % Sum of Obj Value
                sumF = 0;
                
                % Sum of Iterations
                sumI = 0;
                
                % Sum of Function Calls
                sumC = 0;
                
                % Optimum Point [Qc Qf]
                optimum = [0 0];
                
                % Best F
                minF = 0;
                
                % Start timer
                tic;
                
                for l = 1:TestRuns
                    [x,fval,exitflag,output] = m(objfunction,nVars,lb,ub,SwarmSize(i),FunctionTolerance,MaxIterations,SelfAdjustmentWeight(j),SocialAdjustmentWeight(k),UseParallel,UseVectorized);
                    
                    % Add the fval
                    sumF = sumF + fval;
                    
                    % Add the Iterations
                    sumI = sumI + output.iterations;
                    
                    % Add the Function Calls
                    sumC = sumC + output.funccount;
                    
                    % If the solution was found
                    if exitflag == 1
                        
                        % Check if it meets the constraint
                        Th_i = 60+273;
                        Th_o = 45+273;
                        
                        check = -Th_o + (Th_i - (-1*fval)/((x(1)/60000) * 986.877 * 4.181212843576421e+03));
                        
                        % SUCCESS
                        if check < 0
                            % COUNT IT!
                            SuccessfulRuns = SuccessfulRuns + 1;
                            
                            % Check for new optimum
                            if fval < minF || minF == 0
                                minF = fval;
                                optimum = x;
                            end
                            
                        end
                        
                    end
                    
                end
                
                % ------- Save Test Case Results -------
                
                % Current Test Case
                testCaseNumb = (i-1)*n2 + (j-1)*n3 + k;
                
                % Average F value
                avgF = sumF / TestRuns;
                
                % Average Iterations
                avgI = sumI / TestRuns;
                
                % Average Function Calls
                avgC = sumC / TestRuns;
                
                % Save the input conditions to the matrix
                data( testCaseNumb , 1 ) = SwarmSize(i);
                
                % Save the outputs to the matrix
                data( testCaseNumb , 2:9 ) = [SuccessfulRuns avgF avgI avgC toc optimum(:,1) optimum(:,2) minF];
                
            end
        end
    end
    
    %% Create a Table
    
    % Round the matrix values
    data = round(data,3);
    T = table(data(:,1),data(:,2),data(:,3),data(:,4),data(:,5),data(:,6),data(:,7),data(:,8),data(:,9));
    format short;
    T.Properties.VariableNames = labels;
    name = strcat("data/",method,".xlsx");
    writetable(T,name);
end

disp('Done');
