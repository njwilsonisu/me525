clear;
clc;
rng default;

%% Define which PSO method to use

method = 'basic';
%method = 'inertiaWeight';
%method = 'constriction';
%method = 'neighborhood';

m = str2func(method);

%% Setup the Parameters

% For some of the PSO methods, Some of these parameters will be overwritten
% because the method handles their values

% ONLY USED BY BASIC AND INERTIA WEIGHT
if strcmp(method, 'basic') || strcmp(method, 'inertiaWeight')
    % c1
    SelfAdjustmentWeight = [ 1.49 ];
    % c2
    SocialAdjustmentWeight = [ 1.49 ];
else
    % If the method is not basic, the values will be handled
    SelfAdjustmentWeight = [ 0 ];
    SocialAdjustmentWeight = [ 0 ];
end

% Number of times we run each case
TestRuns = 100;

% Upper and Lower Bounds
lb = [0 0];
ub = [10 10];

%Size of the Swarm
SwarmSize = [10 20 50];

%Maximum Number of Iterations
MaxIterations = 500;

UseParallel = false;

UseVectorized = false;

%% Intialize Data Storage Matrix

% Intialize Data Matrix

% Number of Cases
n1 = numel(SwarmSize);
n2 = numel(SelfAdjustmentWeight);
n3 = numel(SocialAdjustmentWeight);

% Each Row represents a run case with the following column setup
labels = {'Swarm Size','c1','c2','Successful Runs','Avg F','Optimum Qc','Optimum Qh','Minimum F'};
data = [n1*n2*n3 , 8];

%% Single Evaluation

% ------- New Test Case -------

% Number of Successful Runs for this Test Case
SuccessfulRuns = 0;

% Sum of Obj Value
sumF = 0;

% Sum of Iterations
sumI = 0;

% Sum of Function Calls
sumC = 0;

% Optimum Point [Qc Qf]
optimum = [0 0];

% Best F
minF = 0;

for l = 1:TestRuns
    [x,fval,exitflag,output] = m(lb,ub,SwarmSize(1),MaxIterations,SelfAdjustmentWeight(1),SocialAdjustmentWeight(1),UseParallel,UseVectorized);
    
    % Add the fval
    sumF = sumF + fval;
    
    % Add the Iterations
    sumI = sumI + output.iterations;
    
    % Add the Function Calls
    sumC = sumC + output.funccount;
    
    % If the solution was found
    if exitflag == 1
        
        % Check if it meets the constraint
        Th_i = 60+273;
        Th_o = 45+273;
        
        check = -Th_o + (Th_i - (-1*fval)/((x(1)/60000) * 986.877 * 4.181212843576421e+03));
        
        % SUCCESS
        if check < 0
            % COUNT IT!
            SuccessfulRuns = SuccessfulRuns + 1;
            
            % Check for new optimum
            if fval < minF || minF == 0
                minF = fval;
                optimum = x;
            end
            
        end
        
    end
    
end

% ------- Save Test Case Results -------

% Average F value
avgF = sumF / TestRuns;

% Average Iterations
avgI = sumI / TestRuns;

% Average Function Calls
avgC = sumC / TestRuns;


fprintf(' -- Test Complete -- \n Successful Runs: %g \n Avg F: %g \n Avg Iterations: %g \n Avg Function Calls: %g \n Optimum: [%0.2f,%0.2f] \n F value: %0.2f \n \n ',SuccessfulRuns,avgF,avgI,avgC,optimum(1),optimum(2),minF);