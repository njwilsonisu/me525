function f=ofun(x)   
% objective function (minimization) 
of=-1*(((exp(((34.7.*x(2) + 34.39.*x(1) - 0.5.*((69.39 .* x(2) - 68.77.*x(1)).^2).^(1./2))./...
    (34.7.*x(2) + 34.39.*x(1) + 0.5.*((69.39.*x(2) - 68.77.*x(1)).^2).^(1./2)) - 1.0)./((4.614./...
    (2147.0.*x(2)).^(4./5)+ 18.86./(4814.0.*x(1)).^(4./5) + 0.002157).*(34.7.*x(2) + 34.39.*x(1)...
    - 0.5.*((69.39.*x(2) - 68.77.*x(1)).^2).^(1./2))))- 1.0).*(1388.0.*x(2) + 1375.0.*x(1) - ...
    20.0.*((69.39.*x(2) - 68.77.*x(1)).^2).^(1./2)))./((exp(((34.7.*x(2) + 34.39.*x(1)- ...
    0.5.*((69.39.*x(2) - 68.77.*x(1)).^2).^(1./2))./(34.7.*x(2) + 34.39.*x(1) + ...
    0.5.*((69.39.*x(2) - 68.77.*x(1)).^2).^(1./2))- 1.0)./((4.614./(2147.0.*x(2)).^(4./5)...
    + 18.86./(4814.0.*x(1)).^(4./5) + 0.002157).*(34.7.*x(2) + 34.39.*x(1)- 0.5.*((69.39.*x(2)...
    - 68.77.*x(1)).^2).^(1./2)))).*(34.7.*x(2) + 34.39.*x(1) - 0.5.*((69.39.*x(2) - ...
    68.77.*x(1)).^2).^(1./2)))./(34.7.*x(2) + 34.39.*x(1) + 0.5.*((69.39.*x(2) - ...
    68.77.*x(1)).^2).^(1./2))-1.0));   
% constraints (all constraints must be converted into <=0 type) 
% if there is no constraints then comments all c0 lines below 
%Qh_max=20;
%Qc_max=20;
Th_inlet=60+273;
Th_threshold=45+273;
c0=[]; 
c0(1)=10*x(1)-1.6*x(2);
% c0(1)= Th_inlet-Th_threshold +(x(1)./60000)*(986.877*4.1812*1e3)*(((exp(((34.7.*x(2) + 34.39.*x(1) - 0.5.*((69.39 .* x(2) - 68.77.*x(1)).^2).^(1./2))./...
%     (34.7.*x(2) + 34.39.*x(1) + 0.5.*((69.39.*x(2) - 68.77.*x(1)).^2).^(1./2)) - 1.0)./((4.614./...
%     (2147.0.*x(2)).^(4./5)+ 18.86./(4814.0.*x(1)).^(4./5) + 0.002157).*(34.7.*x(2) + 34.39.*x(1)...
%     - 0.5.*((69.39.*x(2) - 68.77.*x(1)).^2).^(1./2))))- 1.0).*(1388.0.*x(2) + 1375.0.*x(1) - ...
%     20.0.*((69.39.*x(2) - 68.77.*x(1)).^2).^(1./2)))./((exp(((34.7.*x(2) + 34.39.*x(1)- ...
%     0.5.*((69.39.*x(2) - 68.77.*x(1)).^2).^(1./2))./(34.7.*x(2) + 34.39.*x(1) + ...
%     0.5.*((69.39.*x(2) - 68.77.*x(1)).^2).^(1./2))- 1.0)./((4.614./(2147.0.*x(2)).^(4./5)...
%     + 18.86./(4814.0.*x(1)).^(4./5) + 0.002157).*(34.7.*x(2) + 34.39.*x(1)- 0.5.*((69.39.*x(2)...
%     - 68.77.*x(1)).^2).^(1./2)))).*(34.7.*x(2) + 34.39.*x(1) - 0.5.*((69.39.*x(2) - ...
%     68.77.*x(1)).^2).^(1./2)))./(34.7.*x(2) + 34.39.*x(1) + 0.5.*((69.39.*x(2) - ...
%     68.77.*x(1)).^2).^(1./2))-1.0));
% defining penalty for each constraint 
for i=1:length(c0)     
    if c0(i)>0         
        c(i)=1;     
    else
        c(i)=0;     
    end
end
penalty=1e4;           % penalty on each constraint violation 
f=of+penalty*sum(c);     % fitness function
end
