function [q,withinDesignSpace] = objectiveFunction(Qh,Qc)
% objectiveFunction() - Calculates the theoretical total heat transfer
% for a concentric tube heat exchanger
%
% Input Variables
% Qh = Volumetric flowrate produced by the hot pump
% Qc = Volumetric flowrate produced by the cold pump
%
% Output Variables
% q = theoretical heat transfer rate
% withinDesignSpace = logical variable
%   0 - FAILED: the current design variables are not in the design space
%   1 - SUCCESS: the current design variables are in the design space

%% Characteristic Variables

% User Defined characteristic variables
global L di do Di Do Thi ThoMAX Tci TcoEXP

% support variables
global Th_avg Tc_avg cp_h cp_c rho_h rho_c Ac_h Ac_c mdot_h mdot_c
global Dh_h Dh_c As_h As_c C_h C_c kw h_h h_c Cr NTU UA dt_h dt_c Tho_t Tco_t
global Re_h Re_c effectiveness

% Variable Definitions
% Th_avg = average temperature hot fluid
% Tc_avg  = average temperature cold fluid
% cp_h = specific heat hot fluid
% cp_c = specific heat cold fluid
% rho_h = density hot fluid
% rho_c  = density cold fluid
% Ac_h = cross-sectional area hot fluid
% Ac_c = cross-sectional area cold fluid
% mdot_h = mass flow rate hot fluid
% mdot_c = mass flow rate cold fluid
% Dh_h = hydraulic diameter hot fluid
% Dh_c = hydraulic diameter cold fluid
% As_h = heat exchange area hot fluid
% As_c = heat exchange area cold fluid
% C_h = heat capacity hot fluid
% C_c = heat capacity cold fluid
% kw = thermal conductivty of the wall
% h_h = heat transfer coefficient hot side
% h_c = heat transfer coefficient cold side
% Cr = heat capacity ratio
% NTU = number of transfer units
% UA = overall heat transfer coefficinent times the equivalent exchange area
% effectiveness = effectiveness
% dt_h = theoretical delta T hot side
% dt_c = theoretical delta T cold side
% Tho_t = theoretical outer T hot side
% Tco_t = theoretical outer T cold side
% eff = total efficiency of the system

%% Characteristics of the System

% hot water inlet temperature
Thi = Thi + 273;        % [K]
% MAXIMUM hot water outlet temperature
ThoMAX = 45 + 273;      % [K]

% cold water inlet temperature
Tci = 20 + 273;         % [K]
% EXPECTED cold water outlet temperature
TcoEXP = 35 + 273;      % [K]

% Cross-sectional areas
Ac_h = (pi/4)*di^2;         % [m^2] Cross-sectional area (hot side)
Ac_c = (pi/4)*Di^2-do^2;    % [m^2] Cross-sectional area (cold side)

% Internal tube characteristic length / Annulus hydraulic diameter
Dh_h = di;         % [m] Internal tube characteristic lenght (hot side)
Dh_c = Di-do;         % [m] Annulus hydraulic diameter (cold side)

% Heat exchange areas
As_h = di*pi*L;              % [m^2] Heat exchange area (hot side)
As_c = do*pi*L;              % [m^2] Heat exchange area (cold side)

% thermal conductivity of stainless steel (tube)
kw = 15.1;        % [W/(m*K)]

%% Design Variables

% Convert Q to metric for later use
Qhconverted = Qh / 60000;    % [m^3/s]
Qcconverted = Qc / 60000;    % [m^3/s]

%% Characteristics of the Water through the heat exchanger

% Estimated average temperatures
Tc_avg = (Tci + TcoEXP)/2;        % [K]
Th_avg = (Thi + ThoMAX)/2;        % [K]

% estimated density
rho_h = (-0.0000002301*((Th_avg-273)^4)+0.0000569866*((Th_avg-273)^3)-0.0082923226*((Th_avg-273)^2)+0.0654036947*(Th_avg-273)+999.8017570756);    	% [kg/m^3]
rho_c = (-0.0000002301*((Tc_avg-273)^4)+0.0000569866*((Tc_avg-273)^3)-0.0082923226*((Tc_avg-273)^2)+0.0654036947*(Tc_avg-273)+999.8017570756);      % [kg/m^3]

% estimated specific heat
cp_h = (1.15290498E-12*((Th_avg-273)^6)-3.5879038802E-10*((Th_avg-273)^5)+4.710833256816E-08*((Th_avg-273)^4)-3.38194190874219E-06*((Th_avg-273)^3)+0.000148978977392744*((Th_avg-273)^2)-0.00373903643230733*(Th_avg-273)+4.21734712411944)*1000; % [J/(kg*K)]
cp_c = (1.15290498E-12*((Tc_avg-273)^6)-3.5879038802E-10*((Tc_avg-273)^5)+4.710833256816E-08*((Tc_avg-273)^4)-3.38194190874219E-06*((Tc_avg-273)^3)+0.000148978977392744*((Tc_avg-273)^2)-0.00373903643230733*(Tc_avg-273)+4.21734712411944)*1000; % [J/(kg*K)]

% mass flow rate
mdot_h = Qhconverted*rho_h;       % [m^3/s]
mdot_c = Qcconverted*rho_c;       % [m^3/s]

% interpolate values from thermodynamic properties tables
Tx = [280,285,290,295,300,305,310,315,320,325, ...
    330,335,340,345,350];

mux = [1422,1225,1080,959,855,769,695,631,577, ...
    528,489,453,420,389,365];

kx = [582,590,598,606,613,620,628,634,640,645, ...
    650,656,660,664,668];

% interpolate viscosity
mu_h = interp1(Tx,mux,Th_avg)*10^-6;    % [N-s/m^2]
mu_c = interp1(Tx,mux,Tc_avg)*10^-6;    % [N-s/m^2]

% interpolate thermal conductivity
k_h = interp1(Tx,kx,Th_avg)*10^-3;    % [W/m K]
k_c = interp1(Tx,kx,Tc_avg)*10^-3;    % [W/m K]

% kinematic viscosity
nu_h = mu_h/rho_h;
nu_c = mu_c/rho_c;

% mean velocities
uh = mdot_h/(rho_h*Ac_h);         % [m/s] mean velocity inside the tube
uc = mdot_c/(rho_c*Ac_c);         % [m/s] mean velocity inside the annulus

% Prandtl number
Pr_h = cp_h * mu_h/k_h;
Pr_c = cp_c * mu_c/k_c;

% Reynolds number
Re_h =  (uh*Dh_h)/nu_h;
Re_c =  (uc*Dh_c)/nu_c;


%---------------------------------------------------------
% Nusselt number
%---------------------------------------------------------

% Based on the flow conditions

% Hot Water
% Turbulent flow
if Re_h < 4000
    Nu_h = .0243*(Re_h^(4/5))*Pr_h^(.4);
    %Laminar Flow
else
    % Assuming laminar flow for cold water
    Nu_h = .0265*(Re_h^(4/5))*Pr_h^(.3);
end

% Cold Water
% Turbulent flow
if Re_c < 4000
    Nu_c = .0243*(Re_c^(4/5))*Pr_c^(.4);
    %Laminar Flow
else
    % Assuming laminar flow for cold water
    Nu_c = .0265*(Re_c^(4/5))*Pr_c^(.3);
end

%---------------------------------------------------------
% END
%---------------------------------------------------------

% Heat transfer coefficients
h_h =(Nu_h*k_h)/Dh_h;      % [W/(m^2 K)]
h_c = (Nu_c*k_c)/Dh_c;      % [W/(m^2 K)]

%% e-NTU Analysis

% Resistances
Rw =(log((do/2)/(di/2)))/(kw*2*pi*L) ;     % [K/W] wall resistance
R_h = 1/(h_h*As_h);            % [K/W] convective thermal resistance (tube)
R_c = 1/(h_c*As_c);            % [K/W] convective thermal resistance (annulus)
UA = 1/(R_h+Rw+R_c);             % [W/K] UA

% heat capacity rate
C_h = mdot_h*cp_h;           % [W/K]
C_c =  mdot_c*cp_c;           % [W/K]

% Cmin, Cmax, Cr
C_min = min([C_h,C_c]);          % [W/K]
C_max = max([C_h,C_c]);          % [W/K]
Cr = C_min/C_max;

% NTU
NTU =UA/C_min ;

% counter-flow effectiveness
effectiveness = (1-exp(-NTU*(1-Cr)))/(1-Cr*exp(-NTU*(1-Cr))); % counter-flow equation

% Maximum heat rate
q_max = C_min*(Thi-Tci);    % [W]

% theoretical heat transfer rate
q = effectiveness*q_max;              % [W]

% Delta T and outlet temperatures
dt_h =q/C_h;            % [K]
% Actual hot water outlet temperature
Tho_t = Thi-dt_h;       % [K]

%---------------------------------------------------------
% CHECK IF THE HOT WATER OUTPUT TEMPERATURE IS BELOW THE MAXIMUM
%---------------------------------------------------------
if Tho_t > ThoMAX
    % FAILED
    withinDesignSpace = 0;
else
    % PASSED
    withinDesignSpace = 1;
end
%---------------------------------------------------------
% END
%---------------------------------------------------------

dt_c = q/C_c;           % [K]
% Actual cold water outlet temperature
Tco_t = Tci+dt_c;       % [K]



end