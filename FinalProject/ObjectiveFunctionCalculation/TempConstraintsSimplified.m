function [Tho] = TempConstraintsSimplified(Qh,Qc)
% objectiveFunction() - Calculates the theoretical total energy consumption
% for a concentric tube heat exchanger

% Actual hot water outlet temperature
% Tho_t = Thi-dt_h;       % [K]
% Tho_t = Thi-q/C_h;       % [K]
% Tho_t = Thi-q/(mdot_h*cp_h);       % [K]
% Tho_t = Thi-q/((Qh/60000)*rho_h*cp_h);       % [K]
Tho = 333.0 - (0.01454*(exp(((34.7*Qc + 34.39*Qh - 0.5*((69.39*Qc - 68.77*Qh)^2)^(1/2))/(34.7*Qc + 34.39*Qh + 0.5*((69.39*Qc - 68.77*Qh)^2)^(1/2)) - 1.0)/((4.614/(2147.0*Qc)^(4/5) + 18.86/(4814.0*Qh)^(4/5) + 0.002157)*(34.7*Qc + 34.39*Qh - 0.5*((69.39*Qc - 68.77*Qh)^2)^(1/2)))) - 1.0)*(1388.0*Qc + 1375.0*Qh - 20.0*((69.39*Qc - 68.77*Qh)^2)^(1/2)))/(Qh*((exp(((34.7*Qc + 34.39*Qh - 0.5*((69.39*Qc - 68.77*Qh)^2)^(1/2))/(34.7*Qc + 34.39*Qh + 0.5*((69.39*Qc - 68.77*Qh)^2)^(1/2)) - 1.0)/((4.614/(2147.0*Qc)^(4/5) + 18.86/(4814.0*Qh)^(4/5) + 0.002157)*(34.7*Qc + 34.39*Qh - 0.5*((69.39*Qc - 68.77*Qh)^2)^(1/2))))*(34.7*Qc + 34.39*Qh - 0.5*((69.39*Qc - 68.77*Qh)^2)^(1/2)))/(34.7*Qc + 34.39*Qh + 0.5*((69.39*Qc - 68.77*Qh)^2)^(1/2)) - 1.0));
Tho = Tho - 273;
end