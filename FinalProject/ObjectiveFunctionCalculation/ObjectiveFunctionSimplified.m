function [q,withinDesignSpace] = ObjectiveFunctionSimplified(Qh,Qc)
% objectiveFunction() - Calculates the theoretical total heat transfer of
% the heat exchanger using a simplified symbolic expression

% Input Variables
% Qh = Volumetric flowrate produced by the hot pump
% Qc = Volumetric flowrate produced by the cold pump
%
% Output Variables
% q = theoretical heat transfer rate
% withinDesignSpace = logical variable
%   0 - FAILED: the current design variables are not in the design space
%   1 - SUCCESS: the current design variables are in the design space

% This symbolic expression is only relavent given the following
% characteristic variable values
%
% do = internal diameter inner tube
% do = 0.0095;      % [m]
% % di = external diameter inner tube
% di = 0.0083;      % [m]
% % Di = internal diameter outer tube
% Di = .012;      % [m]
% % Do = external diameter outer tube
% Do = .018;      % [m]
% % L = length of concentric tube
% L = .66;       % [m]
% % hot water inlet temperature
% Thi = 60;           % [C]
% % MAXIMUM hot water outlet temperature
% ThoMAX = 45;        % [C]
% % cold water inlet temperature
% Tci = 20;           % [C]
% % EXPECTED cold water outlet temperature
% TcoEXP = 35;        % [C]

% input variables
global Thi ThoMAX

% theoretical heat transfer rate
q = ((exp(((34.7*Qc + 34.39*Qh - 0.5*((69.39*Qc - 68.77*Qh)^2)^(1/2))/(34.7*Qc + 34.39*Qh + 0.5*((69.39*Qc - 68.77*Qh)^2)^(1/2)) - 1.0)/((4.614/(2147.0*Qc)^(4/5) + 18.86/(4814.0*Qh)^(4/5) + 0.002157)*(34.7*Qc + 34.39*Qh - 0.5*((69.39*Qc - 68.77*Qh)^2)^(1/2)))) - 1.0)*(1388.0*Qc + 1375.0*Qh - 20.0*((69.39*Qc - 68.77*Qh)^2)^(1/2)))/((exp(((34.7*Qc + 34.39*Qh - 0.5*((69.39*Qc - 68.77*Qh)^2)^(1/2))/(34.7*Qc + 34.39*Qh + 0.5*((69.39*Qc - 68.77*Qh)^2)^(1/2)) - 1.0)/((4.614/(2147.0*Qc)^(4/5) + 18.86/(4814.0*Qh)^(4/5) + 0.002157)*(34.7*Qc + 34.39*Qh - 0.5*((69.39*Qc - 68.77*Qh)^2)^(1/2))))*(34.7*Qc + 34.39*Qh - 0.5*((69.39*Qc - 68.77*Qh)^2)^(1/2)))/(34.7*Qc + 34.39*Qh + 0.5*((69.39*Qc - 68.77*Qh)^2)^(1/2)) - 1.0);              % [W]

% Actual hot water outlet temperature
Tho_t = Thi-q/((Qh/60000)*986.877*4.1812*10^3);       % [K]

%---------------------------------------------------------
% CHECK IF THE HOT WATER OUTPUT TEMPERATURE IS BELOW THE MAXIMUM
%---------------------------------------------------------
if Tho_t > ThoMAX
    % FAILED
    withinDesignSpace = 0;
else
    % PASSED
    withinDesignSpace = 1;
end
%---------------------------------------------------------
% END
%---------------------------------------------------------

end