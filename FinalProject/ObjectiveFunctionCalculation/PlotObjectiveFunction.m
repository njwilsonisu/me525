% Nick Wilson, Sai Katamreddy, Ali Rabeh
% ME 525 Optimization Final Project

%% Description

% This code is used to iteratively analyze and graph the total heat transfer 
% of the system for different combinations of the volumetric flow rates

%% Characteristic Variable Initializations

clear
clc

% Declare Global Variables
global L di do Di Do Thi ThoMAX Tci TcoEXP

% Geometrical Parameters

% do = internal diameter inner tube
do = 0.0095;      % [m]
% di = external diameter inner tube
di = 0.0083;      % [m]

% Di = internal diameter outer tube
Di = .012;      % [m]
% Do = external diameter outer tube
Do = .018;      % [m]

% L = length of concentric tube
L = .66;       % [m]

% hot water inlet temperature
Thi = 60;           % [C]
% MAXIMUM hot water outlet temperature
ThoMAX = 45;        % [C]

% cold water inlet temperature
Tci = 20;           % [C]
% EXPECTED cold water outlet temperature
TcoEXP = 35;        % [C]

%% For Single 

% For single test
% Qc = 2;    % [L/min]
% Qh = 4;    % [L/min]
% [q,withinDesignSpace] = objectiveFunction(Qh,Qc);
% 
% printResults;

%% Design Variables

% Volumetric Flow Rate of hot water
Qh = [0:0.1:10];    % [L/min]

% Volumetric Flow Rate of hot water
Qc = [0:0.1:10];    % [L/min]

%% Testing Loop

% define vectors for storing inputs and the objective function output
%Qh value
x = zeros(1,numel(Qh)*numel(Qc));
%Qc value
y = zeros(1,numel(Qh)*numel(Qc));
% resulting q value
z = zeros(1,numel(Qh)*numel(Qc));

% iteration number
i = 1;

% number of successes
S = 0;
% number of failures
F = 0;

for a = 1:numel(Qh)
    for b = 1:numel(Qc)
        % store the design variables of this iteration
        x(i) = Qh(a);
        y(i) = Qc(b);
        % store the objective function outcome
        %[z(i),withinDesignSpace] = objectiveFunction(Qh(a),Qc(b));
        [z(i),withinDesignSpace] = ObjectiveFunctionSimplified(Qh(a),Qc(b));
        
        
        % if the iteration succeeded
        if withinDesignSpace
            S=S+1;
        % if it failed
        else
            F=F+1;
            z(i) = NaN;
        end
        i=i+1;
    end
end

%% Graphing the Results

figure(1)
stem3(x, y, z)
xlabel('Qh [L/min]');
ylabel('Qc [L/min]');
zlabel('q [W]');
grid on
xv = linspace(min(x), max(x), 20);
yv = linspace(min(y), max(y), 20);
[X,Y] = meshgrid(xv, yv);
Z = griddata(x,y,z,X,Y);

% figure(2)
% surf(X, Y, Z);
% xlabel('Qh [L/min]');
% ylabel('Qc [L/min]');
% zlabel('q [W]');
% grid on
% set(gca, 'ZLim',[0 max(z)])
% shading interp