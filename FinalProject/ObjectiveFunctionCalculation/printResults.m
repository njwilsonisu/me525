function printResults
%this function prints all the theoretical results

global effectiveness Qh Qc mdot_h mdot_c Tho_t Tco_t q status


disp(' ')
disp(' -------------------------------------------------------------------')
fprintf('    Counter Flow Arrangement: Theoretical Results\n');
disp(' -------------------------------------------------------------------')

% cprintf('*black', ' Power Input: \n');
% cprintf('*[0.6289,0,0.211]', ' Ph = %5.3f    [W] \n', Ph);
% cprintf('*[0,0.445,0.7383]', ' Pc = %5.3f    [W] \n', Pc);
cprintf('*black', ' Flow Rates: \n');
cprintf('*[0.6289,0,0.211]', ' Qh = %6.3f       \n', Qh);
cprintf('*[0.6289,0,0.211]', ' mdot_h = %6.3f       \n', mdot_h);

cprintf('*[0,0.445,0.7383]', ' Qc = %5.3f      \n', Qc);
cprintf('*[0,0.445,0.7383]', ' mdot_c = %5.3f      \n', mdot_c);

cprintf('*black', ' Effectiveness = %6.3f       \n', effectiveness);

cprintf('*black',' Temperatures: \n');
%cprintf('*[0.6289,0,0.211]', ' Thi = %5.2f       [C]\n', Thi-273);
cprintf('*[0.6289,0,0.211]', ' Tho = %5.2f       [C]\n', Tho_t-273);
%cprintf('*[0,0.445,0.7383]', ' Tci = %5.2f       [C]\n', Tci-273);
cprintf('*[0,0.445,0.7383]', ' Tco = %5.2f       [C]\n', Tco_t-273);

cprintf('*black','Outputs: \n');
cprintf('*black', ' q = %6.3f     [W]\n', q);
cprintf('*black', ' STATUS = %6.3f', status);
disp(' ')
end