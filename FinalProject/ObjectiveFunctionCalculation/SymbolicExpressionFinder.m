% Nick Wilson, Sai Katamreddy, Ali Rabeh
% ME 525 Optimization Final Project

%% Description

% This code uses matlab symbolic variables to find the objective function.
% The final objective function is stored as the value of q.

%% Characteristic Variables

clear
clc

% do = internal diameter inner tube
do = 0.0095;      % [m]
% di = external diameter inner tube
di = 0.0083;      % [m]

% Di = internal diameter outer tube
Di = .012;      % [m]
% Do = external diameter outer tube
Do = .018;      % [m]

% L = length of concentric tube
L = .66;       % [m]

% hot water inlet temperature
Thi = 60+273;       % [K]
% MAXIMUM hot water outlet temperature
ThoMAX = 45+273;       % [K]

% cold water inlet temperature
Tci = 20+273;       % [K]
% EXPECTED cold water outlet temperature
TcoEXP = 35+273;       % [K]

%% Symbolic Function Formulation

%---------------------------------------------------------
% For getting the objective function
%---------------------------------------------------------
syms Qh Qc q Tho_t
% Qh = sym(1); 
% Qc = sym(2);
%---------------------------------------------------------
% END
%---------------------------------------------------------

% Cross-sectional areas
Ac_h = (pi/4)*di^2;              % [m^2] Cross-sectional area (hot side)
Ac_c = (pi/4)*Di^2-do^2;              % [m^2] Cross-sectional area (cold side)

% Internal tube characteristic length / Annulus hydraulic diameter
Dh_h = di;         % [m] Internal tube characteristic lenght (hot side)
Dh_c = Di-do;         % [m] Annulus hydraulic diameter (cold side)

% Heat exchange areas
As_h = di*pi*L;              % [m^2] Heat exchange area (hot side)
As_c = do*pi*L;              % [m^2] Heat exchange area (cold side)

% thermal conductivity of stainless steel (tube)
kw = 15.1;        % [W/(m*K)]

% Convert Q to metric for later use
Qhconverted = Qh / 60000;    % [m^3/s]
Qcconverted = Qc / 60000;    % [m^3/s]

% Estimated average temperatures
Tc_avg = (Tci + TcoEXP)/2;        % [K]
Th_avg = (Thi + ThoMAX)/2;        % [K]

% estimated density
rho_h = (-0.0000002301*((Th_avg-273)^4)+0.0000569866*((Th_avg-273)^3)-0.0082923226*((Th_avg-273)^2)+0.0654036947*(Th_avg-273)+999.8017570756);    	% [kg/m^3]
rho_c = (-0.0000002301*((Tc_avg-273)^4)+0.0000569866*((Tc_avg-273)^3)-0.0082923226*((Tc_avg-273)^2)+0.0654036947*(Tc_avg-273)+999.8017570756);      % [kg/m^3]

% estimated specific heat
cp_h = (1.15290498E-12*((Th_avg-273)^6)-3.5879038802E-10*((Th_avg-273)^5)+4.710833256816E-08*((Th_avg-273)^4)-3.38194190874219E-06*((Th_avg-273)^3)+0.000148978977392744*((Th_avg-273)^2)-0.00373903643230733*(Th_avg-273)+4.21734712411944)*1000; % [J/(kg*K)]
cp_c = (1.15290498E-12*((Tc_avg-273)^6)-3.5879038802E-10*((Tc_avg-273)^5)+4.710833256816E-08*((Tc_avg-273)^4)-3.38194190874219E-06*((Tc_avg-273)^3)+0.000148978977392744*((Tc_avg-273)^2)-0.00373903643230733*(Tc_avg-273)+4.21734712411944)*1000; % [J/(kg*K)]

% mass flow rate
mdot_h = Qhconverted*rho_h;       % [m^3/s]
mdot_c = Qcconverted*rho_c;       % [m^3/s]
vpa(mdot_h,4);
vpa(mdot_c,4);

% interpolate values from thermodynamic properties tables
Tx = [280,285,290,295,300,305,310,315,320,325, ...
    330,335,340,345,350];

mux = [1422,1225,1080,959,855,769,695,631,577, ...
    528,489,453,420,389,365];

kx = [582,590,598,606,613,620,628,634,640,645, ...
    650,656,660,664,668];

% interpolate viscosity
mu_h = interp1(Tx,mux,Th_avg)*10^-6;    % [N-s/m^2]
mu_c = interp1(Tx,mux,Tc_avg)*10^-6;    % [N-s/m^2]

% interpolate thermal conductivity
k_h = interp1(Tx,kx,Th_avg)*10^-3;    % [W/m K]
k_c = interp1(Tx,kx,Tc_avg)*10^-3;    % [W/m K]

% kinematic viscosity
nu_h = mu_h/rho_h;
nu_c = mu_c/rho_c;

% mean velocities
uh = mdot_h/(rho_h*Ac_h);         % [m/s] mean velocity inside the tube
uc = mdot_c/(rho_c*Ac_c);         % [m/s] mean velocity inside the annulus
vpa(uh,4);
vpa(uc,4);

% Prandtl number
Pr_h = cp_h * mu_h/k_h;
Pr_c = cp_c * mu_c/k_c;

% Reynolds number
Re_h =  (uh*Dh_h)/nu_h;
Re_c =  (uc*Dh_c)/nu_c;
vpa(Re_h,4);
vpa(Re_c,4);

%---------------------------------------------------------
% Nusselt number
%---------------------------------------------------------
% Hot Water
% Turbulent flow    
Nu_h = .0243*(Re_h^(4/5))*Pr_h^(.4);
%Laminar Flow
% Nu_h = .0265*(Re_h^(4/5))*Pr_h^(.3);

% Cold Water
% Turbulent flow
% Nu_c = .0243*(Re_c^(4/5))*Pr_c^(.4);
% Laminar Flow
Nu_c = .0265*(Re_c^(4/5))*Pr_c^(.3);

vpa(Nu_h,4);
vpa(Nu_c,4);
%---------------------------------------------------------
% END
%---------------------------------------------------------

% Heat transfer coefficients
h_h =(Nu_h*k_h)/Dh_h;      % [W/(m^2 K)]
h_c = (Nu_c*k_c)/Dh_c;      % [W/(m^2 K)]
vpa(h_h,4);
vpa(h_c,4);

% Resistances
Rw =(log((do/2)/(di/2)))/(kw*2*pi*L) ;     % [K/W] wall resistance
R_h = 1/(h_h*As_h);            % [K/W] convective thermal resistance (tube)
R_c = 1/(h_c*As_c);            % [K/W] convective thermal resistance (annulus)
UA = 1/(R_h+Rw+R_c);             % [W/K] UA
vpa(R_h,4);
vpa(R_c,4);
UA = vpa(UA,4);

% heat capacity rate
C_h = mdot_h*cp_h;           % [W/K]
C_c =  mdot_c*cp_c;           % [W/K]

C_h = vpa(C_h,4);
C_c = vpa(C_c,4);

% Cmin, Cmax, Cr 
% C_min = min([C_h,C_c]);          % [W/K]
C_min = (C_h+C_c-((C_h-C_c)^2)^(1/2))/2;
%C_max = max([C_h,C_c]);          % [W/K]
C_max = (C_h+C_c+((C_h-C_c)^2)^(1/2))/2;
Cr = C_min/C_max;
C_min = vpa(C_min,4);
C_max = vpa(C_max,4);
vpa(Cr,4);

% NTU
NTU =UA/C_min ;
NTU = vpa(NTU,4);
    
% counter-flow effectiveness
effectiveness = (1-exp(-NTU*(1-Cr)))/(1-Cr*exp(-NTU*(1-Cr))); % counter-flow equation
vpa(effectiveness,4);

% Maximum heat rate
q_max = C_min*(Thi-Tci);    % [W]
q_max = vpa(q_max,4);

% theoretical heat rate
q = effectiveness*q_max;              % [W]
q = vpa(q,4);

% Delta T and outlet temperatures
dt_h =q/C_h;            % [K]
% Actual hot water outlet temperature
Tho_t = Thi-dt_h;       % [K]
Tho_t = vpa(Tho_t,4);

dt_c = q/C_c;           % [K]

% Actual cold water outlet temperature
Tco_t = Tci+dt_c;       % [K]
