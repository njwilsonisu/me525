function [q] = calc_rad(T,ep)
% CALC_RAD() - Short function for ESTIMATING the relative heat dissipated
% via RADIATION (see acompanying document for more information)
%
% Syntax:  [w] = calc_rad(T,ep)
%
% Inputs:
%    T - Average temp from FLIR [K]
%    ep - emissivity    [-]
%
% Outputs:
%    q - Estimated heat rate [W]
%
%#ok<*NASGU>
%% MAIN


% Set globals
global sigma As Tinf

% ====================== YOUR CODE HERE ======================
% Instructions: provide the equation to calculate the heat rate for
% radiation (see acompanying document)
% Note the variables names indicated above!

q = ep*sigma*As*(T^4-Tinf^4);    %[W]

% ============================================================

end