function [ks] = calc_ks(m)
% CALC_KS - Returns the unknown thermal conductivity, given the slopes
% from linear regression
%
% Syntax:  [ks] = calc_ks(m)
%
% Inputs:
%    m  - Linear Regression Slopes (vector!)
%
% Outputs:
%    ks  - Computed thermal conductivity of the SAMPLE section (middle)
%
%#ok<*NASGU>
 
%% MAIN

% ======================IMPORTANT NOTE: You need to use these slopes values! ======================
% set derivatives from regression slopes. 
dTdx1 = abs(m(1));  % Top Section
dTdx2 = abs(m(2));  % Specimen
dTdx3 = abs(m(3));  % Bottom Section

% ====================== YOUR CODE HERE ======================
% Supply a value for k_brass
k_brass = ???;  % [W/(m C)]

% ====================== YOUR CODE HERE ======================
% Provide an expression for ks
ks = ???;

end
