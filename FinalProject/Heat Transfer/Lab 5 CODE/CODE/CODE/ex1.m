%% ME 436L Heat Transfer
% Lab 1 | Linear Heat Conduction
%
%  INSTRUCTIONS
%  ------------
%
%  This file contains code that will guide you through the assignment. Be 
%  sure to follow the instructions provided in the attached document.
%
%  NOTE: You will also need to complete the following functions:
%
%       plotData.m
%       calc_ks.m
%       fouriers_law.m
%       calc_contact_res.m
%
%%
% Written By: Spencer Pfeifer | Paola G. Pittoni
% Date:   9/20/18
%
%#ok<*UNRCH>

%% Initialization
clear ; close all; clc
addpath('./lib');
addpath('./data');
raw = [];

disp(' ')
disp(' --------------------------------------------------------')
fprintf('<strong>                Lab 1: Linear Conduction               </strong>\n')
disp(' --------------------------------------------------------')
disp(' ')

%% LOAD DATA

%  We start by first loading a sample dataset
fprintf('Loading data... \n\n');

%----------------YOUR CODE HERE------------
% CHANGE HERE THE FILE NAME TO ANALYZE
% DURING THE IN-CLASS ASSIGNMENT
sname = 'brass.csv';
%------------------------------------------

% load tab separated data into matrix M
M = csvread(sname,15,0);

cprintf('*comments', '>> DONE.\n');

% COMMENT ME OUT!!
break_msg; dbstack; return;

% extract time vector
t = M(:,1);           % [s]

% get temps
dat = M(:,2:9);       % [C]

% get voltage/current
V = M(:,10);          % [V]
I = M(:,11);          % [A]

cprintf('*comments', '>> DONE.\n');

% COMMENT ME OUT!!
break_msg; dbstack; return;

%% =========== Part 0: Warm Up / Systems Check ============= 
% The data has been loaded and separated. Now, let's visualize it
% and make sure the data is at steady-state. If you experience troubles
% completing this section, you may have an outdated version of matlab.

% open figure and plot data
fprintf(['\n\n' '<strong>' 'PART 0: Temperatures vs. Time' '</strong>' '\n\n']);
figure;
h = plot(dat);
set(h, 'LineStyle', '-', 'LineWidth', 2)

% add labels
xlabel('Time [s]');
ylabel('Temperature [C]');
title('Transient Data, T/C','FontSize',16);
grid on

disp(' ');
fprintf('  CHECK: Does your plot match Fig. 1?\n')
fprintf('  CHECK: Is your data at steady-state?\n\n');

cprintf('*comments', '>> DONE.\n');
cprintf('*keyword', '>> No deliverable for this section. \n\n\n');

% COMMENT ME OUT!!
break_msg; dbstack; return;

%% =========== Part 1a: Visualize SS Data ============= 
%  Now, we start our first exercise by visualizing the steady-state
%  Temperature data for each T/C. You will need to complete `plotData.m' 
%  before continuing

fprintf(['\n\n' '<strong>' 'PART 1: Temperature Spatial Distribution' ...
 '</strong>' '\n\n']);

% take an average of the last (~20) points 
N = 20;
Tm = mean(dat(end-N:end,:));      % [C]
Vm = mean(V(end-N:end));          % [V]
Im = mean(I(end-N:end));          % [A]

% set x-vector (position along apparatus)
x = 7.5:15:112.5;       % [mm]   <-- Take Note of UNITS!

% plot data
plotData(x, Tm);

fprintf('  CHECK: Does your plot match Fig. 2?\n\n')
cprintf('*comments', '>> DONE.\n');

% COMMENT ME OUT!!
break_msg; dbstack; return;

%% =========== Part 1b: Add Regression Lines ============= 
% Now, let's clean up our plot a little bit by:
%   - Adding regression lines
%   - Adding slopes to our lines
%
% NOTE: IF you did everything correctly above, you should not need to complete 
%       anything here.

% compute regression & slopes
[m,xv,yv] = set_regression(x,Tm);

% plot with regression lines
plot_full(x,Tm);

cprintf('*comments', '>> DONE.\n');
cprintf('*keyword', '>> Deliverable: Export your figure to an image, and include it in your submission.\n\n\n');

% COMMENT ME OUT!!
break_msg; dbstack; return;

%% =========== Part 2: Calculate Sample k ============= 
% Now, we use the information above to calculate the sample thermal
% conductivity, ks.

fprintf(['\n\n' '<strong>' 'PART 2: Calculate Sample k' '</strong>' '\n\n' ])

% calculate ks
[ks] = calc_ks(m);

% print to screen
str_ks = sprintf('% 5.2f [W/mC]', round(ks, 3));
fprintf(['     ks:' str_ks '\n\n'])


cprintf('*comments', '>> DONE.\n');
cprintf('*keyword', '>> Report your result (including units) and provide 1-2 sentences discussing whether your solution is reasonable or not. \n\n\n');

% COMMENT ME OUT!!
break_msg; dbstack; return;

%% =========== Part 3: Calculate Heat Rate ============= 
% Next, we use Fourier's Law to calculate the heat rate, q, for each
% section.

fprintf(['\n\n' '<strong>' 'PART 3: Calculate Heat Rate' '</strong>' '\n\n'])

% set derivatives from regression slopes
[q] = fouriers_law(ks, m);

% calculate power
P = Vm * Im;

% Round off each value to TWO decimals
P = round(P,2);
qh = round(q(1),2);
qm = round(q(2),2);
qc = round(q(3),2);

% print table to screen
fprintf('  q[W]: \n\n');
T = table(P,qh',qm',qc','VariableNames',{'Pin';'qh';'qs';'qc'});
disp(T); fprintf(' \n\n')

cprintf('*comments', '>> DONE.\n');
cprintf('*keyword', '>> Paste your Command Window output into your submission document.\n');
cprintf('*keyword', '>> Provide 1-2 sentences detailing differences and/or potential loss mechanisms between the Pin and your calculated values. \n\n\n');

% COMMENT ME OUT!!
break_msg; dbstack; return;

%% =========== Part 4: Calculate Contact Resistance ============= 
% Now we can calculate the contact resistance between each section

fprintf(['\n\n' '<strong>' 'PART 4: Calculate Contact Resistance' '</strong>' '\n\n'])

dT(1) = min(yv(:,1))-max(yv(:,2));
dT(2) = min(yv(:,2))-max(yv(:,3));

% calculate contact resistance
[R1, R2] = calc_contact_res(dT, P);

% print to screen
str_R1 = sprintf('% 5.2e [m^2 C/W]', R1);
str_R2 = sprintf('% 5.2e [m^2 C/W]', R2);
fprintf(['    R1:' str_R1 '\n'])
fprintf(['    R2:' str_R2 '\n\n'])

cprintf('*comments', '>> DONE.\n');
cprintf('*keyword', '>> Paste your Command Window output into your submission document.\n');
cprintf('*keyword', '>> Provide 1-2 sentences discussing the factors that may play a role \n   in creating the sharp discontinuities in Fig.3, and what can be used to minimize them. \n\n\n');
