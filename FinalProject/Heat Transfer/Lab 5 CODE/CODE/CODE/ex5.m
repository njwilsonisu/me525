%% ME 436L Heat Transfer
% Heat Exchangers
%
%  INSTRUCTIONS
%  ------------
%
%  This file contains code that will guide you through the assignment. Be
%  sure to follow the instructions provided in the accompanying document.
%
%  NOTE(1): You will need to complete also the following function:
%
%       theoretical.m
%
%%
% Written By: Spencer Pfeifer && Paola G. Pittoni
% Date:   11/02/18
%
%#ok<*SNASGU>
%#ok<*NUSED>
%#ok<*SAGROW>
%#ok<*UNRCH>

%% Initialization
clear ; close all; clc
addpath('./lib');
addpath('./data');

% initialize globals
global Thi Tci arr Th_avg Tc_avg cp_h cp_c rho_h rho_c Ac_h Ac_c 
global mdot_h mdot_c Dh_h Dh_c As_h  As_c C_h C_c do di L kw Di Do

% Thi = inlet temperature hot fluid
% Tci = inlet temperature cold fluid
% arr = flow arrangement
% Th_avg = average temperature hot fluid
% Tc_avg  = average temperature cold fluid
% cp_h = specific heat hot fluid
% cp_c = specific heat cold fluid
% rho_h = density hot fluid
% rho_c  = density cold fluid
% Ac_h = cross-sectional area hot fluid
% Ac_c = cross-sectional area cold fluid
% mdot_h = mass flow rate hot fluid
% mdot_c = mass flow rate cold fluid
% Dh_h = hydraulic diameter hot fluid
% Dh_c = hydraulic diameter cold fluid
% As_h = heat exchange area hot fluid
% As_c = heat exchange area cold fluid
% C_h = heat capacity hot fluid
% C_c = heat capacity cold fluid
% do = internal diameter inner tube
% di = external diameter inner tube
% Di = internal diameter outer tube
% Do = external diameter outer tube
% L = length
% kw = thermal conductivty of the wall

%% 0. Load data

%----------------YOUR CODE HERE------------
% CHANGE HERE YOUR FILE NAME
% comment out the not appropriate
sname = './data/Counter.csv';
%sname = './data/Parallel.csv';

% CHANGE HERE YOUR FLOW ARRANGEMENT
% comment out the not appropriate
arr='Counter';
%arr='Parallel';

% CHANGE HERE YOUR ASSIGNMENT TYPE
% comment out the not appropriate
%assignment='inclass';
assignment='report';
%-------------------------------------------

% Load the data
% delete the fist 14 rows
raw = csvread(sname,15,0);

% keep just the last 25 points (last 25 seconds)
raw(1:end-25,:) = [];

%% 1. Experimental Analysis

%----------------YOUR CODE HERE------------
% Geometrical Parameters
% setup dimensions (check the lab procedures!)
% INSERT HERE YOUR VALUES
L = .66;       % [m]
di = 0.0083;      % [m]
do = 0.0095;      % [m]
Di = .012;      % [m]
Do = .018;      % [m]

Ratio = do/Di;

kw = 15.1;        % [W/(m*K)] thermal conductivity of stainless steel (tube)

% Internal tube characteristic length / Annulus hydraulic diameter
% INSERT HERE YOUR FORMULAS
% Variables names: di, do, Di
Dh_h = di;         % [m] Internal tube characteristic lenght (hot side)
Dh_c = Di-do;         % [m] Annulus hydraulic diameter (cold side)

% Cross-sectional and heat exchange areas
% INSERT HERE YOUR FORMULAS
% Variables names: di, do, Di, L
Ac_h = (pi/4)*di^2;              % [m^2] Cross-sectional area (hot side)
Ac_c = (pi/4)*Di^2-do^2;              % [m^2] Cross-sectional area (cold side)
As_h = di*pi*L;              % [m^2] Heat exchange area (hot side)
As_c = do*pi*L;              % [m^2] Heat exchange area (cold side)
%-------------------------------------------

% Experimental Data Extraction
% volumetric flow rate
Qh_slm = mean(raw(:,8));       % [L/min]
Qc_slm = mean(raw(:,9));       % [L/min]

if strcmpi(arr,'Counter')
% hot side
Thi = mean(raw(:,2))+273;      % [K]
Thm = mean(raw(:,3))+273;      % [K]
Tho = mean(raw(:,4))+273;      % [K]
else
% hot side
Thi = mean(raw(:,4))+273;      % [K]
Thm = mean(raw(:,3))+273;      % [K]
Tho = mean(raw(:,2))+273;      % [K]
end

% cold side
Tci = mean(raw(:,5))+273;      % [K]
Tcm = mean(raw(:,6))+273;      % [K]
Tco = mean(raw(:,7))+273;      % [K]

% average temperatures
Tc_avg = (Tci + Tco)/2;        % [K]
Th_avg = (Thi + Tho)/2;        % [K]

% delta T
Delta_c = Tco - Tci;           % [K]
Delta_h = Thi - Tho;           % [K]

% density
rho_h = (-0.0000002301*((Th_avg-273)^4)+0.0000569866*((Th_avg-273)^3)-0.0082923226*((Th_avg-273)^2)+0.0654036947*(Th_avg-273)+999.8017570756);    	% [kg/m^3]
rho_c = (-0.0000002301*((Tc_avg-273)^4)+0.0000569866*((Tc_avg-273)^3)-0.0082923226*((Tc_avg-273)^2)+0.0654036947*(Tc_avg-273)+999.8017570756);      % [kg/m^3]

% mass flow rate
mdot_h = Qh_slm*rho_h/(1000*60);       % [kg/s]
mdot_c = Qc_slm*rho_c/(1000*60);       % [kg/s]

% specific heat
cp_h = (1.15290498E-12*((Th_avg-273)^6)-3.5879038802E-10*((Th_avg-273)^5)+4.710833256816E-08*((Th_avg-273)^4)-3.38194190874219E-06*((Th_avg-273)^3)+0.000148978977392744*((Th_avg-273)^2)-0.00373903643230733*(Th_avg-273)+4.21734712411944)*1000; % [J/(kg*K)]
cp_c = (1.15290498E-12*((Tc_avg-273)^6)-3.5879038802E-10*((Tc_avg-273)^5)+4.710833256816E-08*((Tc_avg-273)^4)-3.38194190874219E-06*((Tc_avg-273)^3)+0.000148978977392744*((Tc_avg-273)^2)-0.00373903643230733*(Tc_avg-273)+4.21734712411944)*1000; % [J/(kg*K)]


%----------------YOUR CODE HERE------------
%INSERT HERE YOUR FORMULAS!
%Variables names: mdot_h, cp_h, mdot_c, cp_c, C_h, Thi, Tho,_c C, Tco, Tci

% heat capacity rate
C_h = mdot_h*cp_h;           % [W/K]
C_c =  mdot_c*cp_c;           % [W/K]
% Energy Balance
qh = C_h*(Thi-Tho);        % [W]
qc = C_c*(Tco-Tci);        % [W]
qamb = qc-qh;                % [W]
q_amb_pct = qamb/qc*100;     % [%]
%-------------------------------------------

%%  2. Print/Plot Experimental
printexp(Delta_h,Delta_c,qh,qc,qamb,q_amb_pct,Thm,Tho,Tcm,Tco);

if strcmpi(assignment,'inclass')
plotexp(Thm,Tho,Tcm,Tco);
else
%% 4. Theoretical Analysis
[h_h,h_c,Cr,NTU,UA,ep,q_t,dt_h,dt_c,Tho_t,Tco_t]=theoretical;

%% 5. Print/Plot All
printtheo(h_h,h_c,Cr,NTU,UA,ep,q_t,dt_h,dt_c,Tho_t,Tco_t)
plotall(Thm,Tho,Tcm,Tco,Tho_t,Tco_t)
end

