function [q] = fouriers_law(ks, m)
% FOURIERS_LAW -  Receives the sample ks and the temperature gradients,
% m, for each section. Returns the heat rate, q (as a VECTOR, in [W]),
% for each section in the linear heat conduction apparatus.
%
% Syntax:  [q] = fouriers_law(ks, m)
%
% Inputs:
%    ks - thermal conductivity of the SAMPLE section (middle) [W/mC]
%    m  - Linear Regression Slopes (vector!)
%
% Outputs:
%    q  - Heat Rate (Vector!) [W]
%
%% MAIN

% ===IMPORTANT NOTE: You need to use these slopes values! ===
% set derivatives from regression slopes.
dTdx1 = abs(m(1));  % Top Section
dTdx2 = abs(m(2));  % Specimen
dTdx3 = abs(m(3));  % Bottom Section

% ====================== YOUR CODE HERE ======================
% Set known material parameters
d = ???;                 % [mm]
k_brass = ???;           % [W/(m C)]

% ====================== YOUR CODE HERE ======================
% calculate area
A = ???;                 % [m^2] <-- NOTE UNITS!!!!

% ====================== YOUR CODE HERE ======================
% Provide an expression for each q [W]
% Hint: Be sure to check your units!
% (super hint: the units of the dT/dx are... check your graph!)
q(1) = ???;    % [W]
q(2) = ???;    % [W]
q(3) = ???;    % [W]

end

