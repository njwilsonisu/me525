%% ME 436L Heat Transfer
% Lab| Extended Surfaces: Radiation
%
%  INSTRUCTIONS
%  ------------
%
%  This file contains code that will guide you through the RADIATION
%  portion of the assignment. Be sure to follow the instructions provided
%  in the attached document.
%
%  This script performs the following operations:
%
%       - Estimates the relative heat lost to radiation and convection
%       - Computes percentages and plots a bar graph for comparison
%
%    NOTE: You will need to complete the following functions:
%
%       calc_rad.m
%       calc_conv.m
%
%%
% Written By: Spencer Pfeifer | Paola G. Pittoni
% Date:   9/7/18
%
%#ok<*SNASGU>
%#ok<*NUSED>
%#ok<*SAGROW>
%#ok<*UNRCH>

%% Initialization
clear ; close all; clc
addpath('./lib');
raw = [];

global IF_PRINT_TABLES
IF_PRINT_TABLES = 1;

disp(' ')
disp(' --------------------------------------------------------')
fprintf('<strong>            Extended Surfaces | Radiation               </strong>\n')
disp(' --------------------------------------------------------')
disp(' ')

%% =========== Part 1: Input Properties ============= 
% Now, using the procedures document for reference, fill in the necessary
% information below (note: ep and Tavg inserted just in-class!)

fprintf(['<strong>' 'PART 1:' '</strong>'])
fprintf(' Set Properties. \n');

% set globals
global sigma As Tinf

% set 1
% INSERT HERE YOUR CODE
h(1) = 9;    % [W/m^2 C]
ep(1) = 0.82;                 %CHANGE THIS VALUE IN-CLASS
Tavg(1) = 327.5714;  % [K]    %CHANGE THIS VALUE IN-CLASS

% set 2
% INSERT HERE YOUR CODE
h(2) = 30;    % [W/m^2 C]
ep(2) = 0.82;                 %CHANGE THIS VALUE IN-CLASS
Tavg(2) = 323.7888;  % [K]    %CHANGE THIS VALUE IN-CLASS

% set 3
% INSERT HERE YOUR CODE
h(3) = 40;    % [W/m^2 C]
ep(3) = 0.82;                 %CHANGE THIS VALUE IN-CLASS
Tavg(3) = 322.4360;  % [K]    %CHANGE THIS VALUE IN-CLASS

% ***INSERT YOU CODE HERE***
% Fin Properties
D = .01;                   % (Diameter) [m]

% ***INSERT YOU CODE HERE***
% Note: we assume **1/3** of the length!
L = .116667;                   % Lenght considered [m]
As = .003616667;                  % Surface area [m^2]

% ***INSERT YOU CODE HERE***
% boltzmann
sigma = 5.67 * 10^-8;               % [W/(m^2 K^4)]

% Set Tinf -- as measured in lab
Tinf = 23.5 + 273;     % [K]  %CHANGE THIS VALUE IN-CLASS

disp(' ');
cprintf('comments', '>> DONE.\n');

%% =========== Part 2: Loop over data & Plot ============= 
% Now, this section performs the computations and plots our data. This is
% done by looping over each dataset. Therefore, you must complete each of
% the functions above before continuting on.

% loop over stations
for ii = 1:length(Tavg)
    
    % radiation & convection estimations
    q_rad(ii) = calc_rad(Tavg(ii),ep(ii));
    q_conv(ii)= calc_conv(Tavg(ii),h(ii));
    
    pct_rad(ii) = q_rad(ii)/( q_conv(ii) + q_rad(ii)) * 100;
end

%% PRINT FIGURES & TABLES

% plotting
plot_bargraph;

% print figs
if ispc  % if windows
    print('figs/rad_estimation', '-dtiff','-r150');
else
    print('figs/rad_estimation', '-dpng','-r150');
end

% print table
T1 = set_array(q_conv, q_rad, pct_rad, 'round', 3);
descr = ' Heat Transfer Rate [W]:';
var_names = {'CONV';'RAD';'PCT_RAD'};
lbl = {'Free';'Med';'High';};
print_table(T1, descr, lbl, var_names, 'radiation');

