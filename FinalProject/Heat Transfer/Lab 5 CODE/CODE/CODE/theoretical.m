function [h_h,h_c,Cr,NTU,UA,ep,q_t,dt_h,dt_c,Tho_t,Tco_t]=theoretical
% THEORETICAL() - Calculates the theoretical h (tube and annulus sides),
% the theoretical outlet temperatures and the
% theoretical heat transfer for a concentric tube heat exchanger
% (adiabatic)
%
% Syntax:  [h_h,h_c,Cr,NTU,UA,ep,q_t,dt_h,dt_c,Tho_t,Tco_t]=theoretical
%
% Outputs:
% h_h = heat transfer coefficient hot side
% h_c = heat transfer coefficient cold side
% Cr = heat capacity ratio
% NTU = number of transfer units
% UA = overall heat transfer coefficinent times the equivalent exchange area
% ep = effectiveness
% q_t = theoretical heat transfer rate
% dt_h = theoretical delta T hot side
% dt_c = theoretical delta T cold side
% Tho_t = theoretical outer T hot side
% Tco_t = theoretical outer T cold side
%
%#ok<*NASGU>
%% MAIN

global Thi Tci rho_h rho_c Ac_h Ac_c arr
global mdot_h mdot_c Dh_h Dh_c As_h  As_c C_h C_c do di L kw

%% Additional properties needed
[k_h,k_c,Pr_h,Pr_c,nu_h,nu_c]=prop;

%% Reynolds number and heat transfer coefficients (annulus and tube)

%----------------YOUR CODE HERE------------
% Variables names: mdot_h,mdot_c,rho_h,rho_c,Ac_h,Ac_c,Dh_h,Dh_c,nu_h,nu_c,
% Pr_h, Pr_hc, k_h, k_c, As_h, As_c,do,di,L,kw,As_h, As_c,Thi,Tci

% INSERT HERE YOUR FORMULAS
% mean velocities
uh = mdot_h/(rho_h*Ac_h);         % [m/s] mean velocity inside the tube
uc = mdot_c/(rho_c*Ac_c);         % [m/s] mean velocity inside the annulus

% INSERT HERE YOUR FORMULAS
% Reynolds number
Re_h =  (uh*Dh_h)/nu_h;
Re_c =  (uc*Dh_c)/nu_c;

% INSERT HERE YOUR FORMULAS
% Nusselt number
Nu_h = .0243*(Re_h^(4/5))*Pr_h^(.4);
Nu_c = .0265*(Re_c^(4/5))*Pr_c^(.3);

% INSERT HERE YOUR FORMULAS
% Heat transfer coefficients
h_h =(Nu_h*k_h)/Dh_h ;      % [W/(m^2 K)]
h_c = (Nu_c*k_c)/Dh_c;      % [W/(m^2 K)]

%% e-NTU Analysis

% INSERT HERE YOUR FORMULAS
% Resistances
Rw =(log((do/2)/(di/2)))/(kw*2*pi*L) ;     % [K/W] wall resistance
R_h = 1/(h_h*As_h);            % [K/W] convective thermal resistance (tube)
R_c = 1/(h_c*As_c);            % [K/W] convective thermal resistance (annulus)
UA = 1/(R_h+Rw+R_c);             % [W/K] UA

% Cmin, Cmax, Cr 
C_min = min([C_h,C_c]);          % [W/K]
C_max = max([C_h,C_c]);          % [W/K]
Cr = C_min/C_max;

% INSERT HERE YOUR FORMULAS
% NTU, epsilon
NTU =UA/C_min ;

if strcmpi(arr,'Counter')
    
% INSERT HERE YOUR FORMULAS
% counter-flow effectiveness
ep = (1-exp(-NTU*(1-Cr)))/(1-Cr*exp(-NTU*(1-Cr))); %counter-flow equation

else
    
% INSERT HERE YOUR FORMULAS
% parallel-flow effectiveness
ep = (1-exp(-NTU*(1+Cr)))/(1+Cr); %parallel-flow equation

end

% INSERT HERE YOUR FORMULAS
% theoretical heat rate
q_max = C_min*(Thi-Tci);    % [W]
q_t =ep*q_max;               % [W]

% INSERT HERE YOUR FORMULAS
% theoretical Delta T and outlet temperatures
dt_h =q_t/C_h ;           % [K]
Tho_t = Thi-dt_h;       % [K]

dt_c = q_t/C_c ;          % [K]
Tco_t = Tci+dt_c;       % [K]
% dt_h = theoretical delta T hot side
% dt_c = theoretical delta T cold side
% Tho_t = theoretical outer T hot side
% Tco_t = theoretical outer T cold side

%-------------------------------------------

end