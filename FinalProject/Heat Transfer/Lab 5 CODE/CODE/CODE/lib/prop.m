function [k_h,k_c,Pr_h,Pr_c,nu_h,nu_c]=prop
%This function interpolates more needed properties for the theoretical
%analysis

global Th_avg Tc_avg cp_h cp_c rho_h rho_c

% More water propeties needed for the analysis
% Properties from Table A.6 pg. 1003
Tx = [280,285,290,295,300,305,310,315,320,325, ...
    330,335,340,345,350];

mux = [1422,1225,1080,959,855,769,695,631,577, ...
    528,489,453,420,389,365];

kx = [582,590,598,606,613,620,628,634,640,645, ...
    650,656,660,664,668];

% interpolate viscosity
mu_h = interp1(Tx,mux,Th_avg)*10^-6;    % [N-s/m^2]
mu_c = interp1(Tx,mux,Tc_avg)*10^-6;    % [N-s/m^2]

% interpolate thermal conductivity
k_h = interp1(Tx,kx,Th_avg)*10^-3;    % [W/m K]
k_c = interp1(Tx,kx,Tc_avg)*10^-3;    % [W/m K]

% Calculate Pr (from definition)
Pr_h = cp_h * mu_h/k_h;
Pr_c = cp_c * mu_c/k_c;

% Calculate nu (from definition)
nu_h = mu_h/rho_h;
nu_c = mu_c/rho_c;