function plotall(Thm,Tho,Tcm,Tco,Tho_t,Tco_t)
%this function plots all the experimental and theoretical results

global Thi Tci  arr L

titl=sprintf('%s Flow',arr);

x = [1,2,3];
    Th = [Thi,Thm,Tho];
if strcmpi(arr,'Counter')
    Tc = [Tco,Tcm,Tci];
else
    Tc = [Tci,Tcm,Tco];
end
% plotting colors
bl = [0 114 189]./256;       % parula blue
rd = [161 0 31]./256;        % parula red
org = [217 83 25]./256;      % parula orange
ppl = [106 81 163]./256;     % purple

% hot
figure;
h = plot(x,Th-273,'-o');
set(h,'Color', rd, 'MarkerSize',7,'MarkerFaceColor',rd,'LineWidth',1.5);
hold on
h = plot([1,3],[Thi,Tho_t]-273,'--o');
set(h,'Color', rd, 'MarkerSize',7,'LineWidth',1.5);
% cold
h = plot(x,Tc-273,'-o');
set(h,'Color', bl, 'MarkerSize',7,'MarkerFaceColor',bl,'LineWidth',1.5);

if strcmpi(arr,'Counter')
h = plot([1,3],[Tco_t,Tci]-273,'--o');
else
h = plot([1,3],[Tci,Tco_t]-273,'--o');
end
set(h,'Color', bl, 'MarkerSize',7,'LineWidth',1.5);
% x ticks
NumTicks = 7;
L = get(gca,'XLim');
set(gca,'XTick',linspace(L(1),L(2),NumTicks))
set(gca,'XTickLabel',{'1  ' ; ' ' ; ' ' ; ' ' ; ' '; ' ';'2'})

ylim([0, 70])

% labels
xlabel('x','FontSize',16);
ylabel('Temperature [C]','FontSize',16);
title(titl,'FontSize',20);
legend('T_{h, exp}', 'T_{h, theor}','T_{c, exp}', 'T_{c, theor}', 'Location','Southeast')
grid on
end