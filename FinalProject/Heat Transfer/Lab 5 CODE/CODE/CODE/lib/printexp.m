function printexp(Delta_h,Delta_c,qh,qc,qamb,q_amb_pct,Thm,Tho,Tcm,Tco)
%this function prints all the experimental data / results

global Thi Tci Th_avg Tc_avg arr
global mdot_h mdot_c C_h C_c

disp(' ')
disp(' -------------------------------------------------------------------')
fprintf('    %s Flow Arrangement: Experimental Results\n',arr);
disp(' -------------------------------------------------------------------')
disp(' ')

cprintf('*black','Mass Flow Rates: \n');
cprintf('*[0.6289,0,0.211]', ' mdot_h = %5.4f    [kg/s] \n', mdot_h);
cprintf('*[0,0.445,0.7383]', ' mdot_c = %5.4f    [kg/s] \n', mdot_c);
disp(' ')
cprintf('*black','Temperatures (Experimental): \n');
cprintf('*[0.6289,0,0.211]', ' Thi = %5.2f       [C]\n', Thi-273);
cprintf('*[0.6289,0,0.211]', ' Thm = %5.2f       [C]\n', Thm-273);
cprintf('*[0.6289,0,0.211]', ' Tho = %5.2f       [C]\n', Tho-273);
cprintf('*[0.6289,0,0.211]', ' Th_avg = %5.2f    [C]\n', Th_avg-273);
cprintf('*[0,0.445,0.7383]', ' Tci = %5.2f       [C]\n', Tci-273);
cprintf('*[0,0.445,0.7383]', ' Tcm = %5.2f       [C]\n', Tcm-273);
cprintf('*[0,0.445,0.7383]', ' Tco = %5.2f       [C]\n', Tco-273);
cprintf('*[0,0.445,0.7383]', ' Tc_avg = %5.2f    [C]\n', Tc_avg-273);
disp(' ')
cprintf('*black','Delta T (Experimental): \n');
cprintf('*[0.6289,0,0.211]', ' Delta_h = %5.2f   [C]\n', Delta_h);
cprintf('*[0,0.445,0.7383]', ' Delta_c = %5.2f   [C]\n', Delta_c);
disp(' ')
cprintf('*black','Heat Capacity Rates: \n');
cprintf('*[0.6289,0,0.211]', ' C_h = %5.3f       [W/K]\n', C_h);
cprintf('*[0,0.445,0.7383]', ' C_c = %5.3f       [W/K]\n', C_c);
disp(' ')
cprintf('*black','Heat Transfer Rates (Experimental): \n');
cprintf('*[0.6289,0,0.211]', ' qh_exp = %5.2f       [W]\n', qh);
cprintf('*[0,0.445,0.7383]', ' qc_exp = %5.2f       [W]\n', qc);
cprintf('*black', ' qamb = %6.2f         [W]\n', qamb);
cprintf('*black', ' q_amb_pct = %6.3f    [%%]\n', q_amb_pct);
disp(' ')
end