function printtheo(h_h,h_c,Cr,NTU,UA,ep,q_t,dt_h,dt_c,Tho_t,Tco_t)
%this function prints all the theoretical results

global Thi Tci arr

disp(' ')
disp(' -------------------------------------------------------------------')
fprintf('    %s Flow Arrangement: Theoretical Results\n',arr);
disp(' -------------------------------------------------------------------')
disp(' ')

cprintf('*black','Heat transfer coefficients (Theoretical): \n');
cprintf('*[0.6289,0,0.211]', ' h_h = %5.3f    [W/(m^2 K)] \n', h_h);
cprintf('*[0,0.445,0.7383]', ' h_c = %5.3f    [W/(m^2 K)] \n', h_c);
disp(' ')
cprintf('*black','epsilon-NTU analysis results: \n');
cprintf('*black', ' Cr = %6.3f       \n', Cr);
cprintf('*black', ' NTU = %5.3f      \n', NTU);
cprintf('*black', ' UA = %6.3f       [W/K]\n', UA);
cprintf('*black', ' ep = %6.3f       \n', ep);

disp(' ')
cprintf('*black','Temperatures (Theoretical): \n');
cprintf('*[0.6289,0,0.211]', ' Thi = %5.2f       [C]\n', Thi-273);
cprintf('*[0.6289,0,0.211]', ' Tho = %5.2f       [C]\n', Tho_t-273);
cprintf('*[0,0.445,0.7383]', ' Tci = %5.2f       [C]\n', Tci-273);
cprintf('*[0,0.445,0.7383]', ' Tco = %5.2f       [C]\n', Tco_t-273);
disp(' ')
cprintf('*black','Delta T (Theoretical): \n');
cprintf('*[0.6289,0,0.211]', ' dt_h = %5.2f      [C]\n', dt_h);
cprintf('*[0,0.445,0.7383]', ' dt_c = %5.2f      [C]\n', dt_c);
disp(' ')
cprintf('*black','Heat Transfer Rate (Theoretical): \n');
cprintf('*black', ' q_t = %6.3f     [W]\n', q_t);
disp(' ')
end