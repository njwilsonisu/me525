tic
clc;
clear all;
close all;
rng default

runcase=1;
if runcase==1
    objectiveFunction=@ofun1;
    LB=[-3 -2];         %lower bounds of variables
    UB=[3 2];      %upper bounds of variables
    analytical_solution=-1.031628;
elseif runcase==2
    objectiveFunction=@ofun2;
    LB=[-2 -2 -2 -2 -2];         %lower bounds of variables
    UB=[2 2 2 2 2];      %upper bounds of variables
    analytical_solution=0;
elseif runcase==3
    objectiveFunction=@ofun3;
    LB=[-10 -10 -10 -10 -10];         %lower bounds of variables
    UB=[10 10 10 10 10];      %upper bounds of variables
    analytical_solution=0;
end
    
m=length(LB);   % number of variables
c1Tiral=1;
c2Tiral=1;
vMaxTiral=1;

c1Vals=[0.1 0.6 1.1  1.6 2.1 2.6];
c2Vals=[0.1 0.6 1.1  1.6 2.1 2.6];
vMaxVals=[1e-2 2.5e-2 5e-2 7.5e-2 1e-1];

numSwarms=25;          % population size
c1=c1Vals(c1Tiral);           % acceleration factor
c2=c2Vals(c2Tiral);           % acceleration factor
% pso main program----------------------------------------------------start
maxite=100;    % set maximum number of iteration 
maxrun=100;      % set maximum number of runs need to be
vMax=vMaxVals(vMaxTiral);  % start at 10% of xmin-xmax 

successfulRuns=0;
toleranceCiterionValue=1e-6;
numberObjEvaluations=0;
for run=1:maxrun
    run
    % pso initialization----------------------------------------------start
    for i=1:numSwarms
        for j=1:m
            x0(i,j)=round(LB(j)+rand()*(UB(j)-LB(j)));
        end
    end
    x=x0;       % initial population
    v=0*x0; % initial velocity
    for i=1:numSwarms
        f0(i,1)=objectiveFunction(x0(i,:));
        numberObjEvaluations=numberObjEvaluations+1;
    end
    [fmin0,index0]=min(f0);    
    pbest=x0;               % initial pbest
    gbest=x0(index0,:);     % initial gbest    
    % pso algorithm---------------------------------------------------start
    ite=1;    
    tolerance=1;
    while ite<=maxite && tolerance>=toleranceCiterionValue
        % pso velocity updates
        for i=1:numSwarms
            for j=1:m
                v(i,j)=(v(i,j)+c1*rand()*(pbest(i,j)-x(i,j))...
                    +c2*rand()*(gbest(1,j)-x(i,j)));
                if v(i,j)>vMax
                    v(i,j)=vMax;
                end    
            end
        end
        % pso position update
        for i=1:numSwarms
            for j=1:m
                x(i,j)=x(i,j)+v(i,j);
            end
        end
        % handling boundary violations
        for i=1:numSwarms
            for j=1:m
                if x(i,j)<LB(j)
                    x(i,j)=LB(j);
                elseif x(i,j)>UB(j)
                    x(i,j)=UB(j);
                end
            end
        end
        % evaluating fitness
        for i=1:numSwarms
            f(i,1)=objectiveFunction(x(i,:));
            numberObjEvaluations=numberObjEvaluations+1;
        end
        % updating pbest and fitness
        for i=1:numSwarms
            if f(i,1)<f0(i,1)
                pbest(i,:)=x(i,:);
                f0(i,1)=f(i,1);
            end
        end
        [fmin,index]=min(f0);   % finding out the best particle
        ffmin(ite,run)=fmin;    % storing best fitness
        ffite(run)=ite;         % storing iteration count
        % updating gbest and best fitness
        if fmin<fmin0
            gbest=pbest(index,:);
            fmin0=fmin;
        end    
        % calculating tolerance
        if ite>1
            tolerance=abs(analytical_solution-fmin0);
        end    
        ite=ite+1;
    end
    if ite<=maxite
        successfulRuns=successfulRuns+1;
        gbest_successful=pbest(index,:);
        fmin0=fmin;
    end
    
    % pso algorithm-----------------------------------------------------end
    fvalue=objectiveFunction(gbest);
    numberObjEvaluations=numberObjEvaluations+1;
    fff(run)=fvalue;
    rgbest(run,:)=gbest;
end
[bestfun,bestrun]=min(fff);
best_variables=rgbest(bestrun,:);
disp(sprintf('run number    iterations count    accuracy'));
for i=1:maxrun
    disp(sprintf('    %1g            %1g            %8.6f',i,ffite(i),abs(fff(i)-analytical_solution)));    
end
totalIterations=0;
for i=1:maxrun
    totalIterations = totalIterations+ffite(i);    
end
averageIterationsPerRun=totalIterations/maxrun;

maxerror=0;
accuracy=zeros(maxrun);
for i=1:maxrun
    accuracy(i) = abs(fff(i)-analytical_solution);  
    if accuracy(i)>maxerror
        maxerror=accuracy(i);
    end
end
disp(sprintf('Number of successful runs is %1g and the average number of iterations per run is %8.2f',successfulRuns,averageIterationsPerRun));
disp(sprintf('Maximum error is %8.6f and the number of objective value evaluations is %1g',maxerror,numberObjEvaluations));
disp(sprintf('*********************************************************'));
disp(sprintf('Total time elapsed (in seconds) is %8.4f',toc));

function f1=ofun1(x)   
% objective function (minimization)
f1 = (4-2.1*x(1).^2 +(1.0/3.0)*x(1).^4).*x(1).^2 +x(1).*x(2)+ (-4+4*x(2).^2).*x(2).^2;
end

function f2=ofun2(x)   
% objective function (minimization)
aVal=20;bVal=0.2;cVal=2*pi;
f2 = (-aVal*exp(-bVal*sqrt((1.0/5.0)*(x(:,1).^2+x(:,2).^2+x(:,3).^2+x(:,4).^2+x(:,5).^2)))...
    -exp((1.0/5.0)*(cos(cVal*x(:,1))+cos(cVal*x(:,2))+cos(cVal*x(:,3))+cos(cVal*x(:,4))+cos(cVal*x(:,5))))+aVal+exp(1));
% set the position of the initial swarm
end

function f3=ofun3(x)   
% objective function (minimization)
f3 =(((1.0/4000.0)*(x(:,1).^2+x(:,2).^2+x(:,3).^2+x(:,4).^2+x(:,5).^2))...
    -cos(x(:,1)/sqrt(1)).*cos(x(:,2)/sqrt(2)).*cos(x(:,3)/sqrt(3)).*cos(x(:,4)/sqrt(4)).*cos(x(:,5)/sqrt(5))+1);
end


