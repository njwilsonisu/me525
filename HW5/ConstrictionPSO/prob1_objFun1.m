clc;clear all;close all;
xLeft=-3;
xRight=3;
yLeft=-2;
yRight=2;
a = xLeft:xRight-1;
numberSwarmsInOneDirection=xRight-xLeft;
swarm_size = numberSwarmsInOneDirection^2;                       % number of the swarm particles
[X,Y] = meshgrid(a,a);
C = cat(2,X',Y');
D = reshape(C,[],2);
maxIter = 10;                          % maximum number of iterations
correction_factor = 2.05;
phi=correction_factor+correction_factor;
k=2.0/abs(2-phi-sqrt(phi^2-4*phi));
Nx=100;
Ny=100;
xVar=linspace(xLeft,xRight,Nx);
yVar=linspace(yLeft,yRight,Ny);
%% define the objective funcion here (vectorized form)
objfcn = @(x)(4-2.1*x(:,1).^2 +(1.0/3.0)*x(:,1).^4).*x(:,1).^2 +x(:,1).*x(:,2)+ (-4+4*x(:,2).^2).*x(:,2).^2;

% set the position of the initial swarm
swarm(1:swarm_size,1,1:2) = D;          % set the position of the particles in 2D
swarm(:,2,:) = 0;                       % set initial velocity for particles
swarm(:,4,1) = 1000;                    % set the best value so far
plotObjFcn = 1;                         % set to zero if you do not need a final plot
tic;
%% The main loop of PSO

for iter = 1:maxIter
    swarm(:, 1, 1) = swarm(:, 1, 1) + swarm(:, 2, 1)/1.3;       %update x position with the velocity
    swarm(:, 1, 2) = swarm(:, 1, 2) + swarm(:, 2, 2)/1.3;       %update y position with the velocity
    x = swarm(:, 1, 1);                                         % get the updated position
    y = swarm(:, 1, 2);                                         % updated position
    fval = objfcn([x y]);                                       % evaluate the function using the position of the particle
    
    % compare the function values to find the best ones
    for ii = 1:swarm_size
        if fval(ii,1) < swarm(ii,4,1)
            swarm(ii, 3, 1) = swarm(ii, 1, 1);                  % update best x position,
            swarm(ii, 3, 2) = swarm(ii, 1, 2);                  % update best y postions
            swarm(ii, 4, 1) = fval(ii,1);                       % update the best value so far
        end
    end
    
    [~, gbest] = min(swarm(:, 4, 1));                           % find the best function value in total
    
    % update the velocity of the particles
    swarm(:, 2, 1) = k*(swarm(:, 2, 1) + correction_factor*(rand(swarm_size,1).*(swarm(:, 3, 1) ...
        - swarm(:, 1, 1))) + correction_factor*(rand(swarm_size,1).*(swarm(gbest, 3, 1) - swarm(:, 1, 1))));   %x velocity component
    swarm(:, 2, 2) = k*(swarm(:, 2, 2) + correction_factor*(rand(swarm_size,1).*(swarm(:, 3, 2) ...
        - swarm(:, 1, 2))) + correction_factor*(rand(swarm_size,1).*(swarm(gbest, 3, 2) - swarm(:, 1, 2))));   %y velocity component
    
    % plot the particles
    clf;plot(swarm(:, 1, 1), swarm(:, 1, 2), 'bx');             % drawing swarm movements
    axis([-3 3 -2 2]);
    pause(.1);                                                 % un-comment this line to decrease the animation speed
    disp(['iteration: ' num2str(iter)]);
end
toc

[gbest,index] = min(swarm(:, 4, 1));
xmin=swarm(index, 3, 1);
ymin=swarm(index, 3, 2);
disp(['Minimum = ' num2str(gbest) ' at (x,y) = (',num2str(xmin),', ',num2str(ymin),')']);

%% plot the function
if plotObjFcn
    ub = 40;
    lb = -40;
    npoints = 1000;
    x = (ub-lb) .* rand(npoints,2) + lb;
    for ii = 1:npoints
        f = objfcn([x(ii,1) x(ii,2)]);
        plot3(x(ii,1),x(ii,2),f,'.r');hold on
    end
    plot3(swarm(1,3,1),swarm(1,3,2),swarm(1,4,1),'xb','linewidth',5,'Markersize',5);grid
end

objFuncArray=zeros(Nx,Ny);
for i=1:Nx
    for j=1:Ny
        objFuncArray(i,j)=objfcn([xVar(i) yVar(j)]);
    end
end
figure(2);
surf(xVar,yVar,objFuncArray);
xlabel('x');
ylabel('y');
title('objective Function');