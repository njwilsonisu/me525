clc;clear all;close all;
x1Left=-2;x1Right=2;
x2Left=-2;x2Right=2;
x3Left=-2;x3Right=2;
x4Left=-2;x4Right=2;
x5Left=-2;x5Right=2;
a = x1Left:x1Right-1;
numberSwarmsInOneDirection=x1Right-x1Left;
swarm_size = numberSwarmsInOneDirection^5;                       % number of the swarm particles
[X1,X2,X3,X4,X5] = ndgrid(a,a,a,a,a); %meshgrid([a,a,a,a,a]);
C = cat(2,X1,X2,X3,X4,X5);
D = reshape(C,[],5);
maxIter = 10;                          % maximum number of iterations
correction_factor = 2.05;
phi=correction_factor+correction_factor;
k=2.0/abs(2-phi-sqrt(phi^2-4*phi));
Nx=100;
Ny=100;
xVar=linspace(x1Left,x1Right,Nx);
yVar=linspace(x2Left,x2Right,Ny);
%% define the objective funcion here (vectorized form)
aVal=20;bVal=0.2;cVal=2*pi;
objfcn = @(x)(-aVal*exp(-bVal*sqrt((1.0/5.0)*(x(:,1).^2+x(:,2).^2+x(:,3).^2+x(:,4).^2+x(:,5).^2)))...
    -exp((1.0/5.0)*(cos(cVal*x(:,1))+cos(cVal*x(:,2))+cos(cVal*x(:,3))+cos(cVal*x(:,4))+cos(cVal*x(:,5))))+aVal+exp(1));
% set the position of the initial swarm
swarm(1:swarm_size,1,1:5) = D;          % set the position of the particles in 2D
swarm(:,2,:) = 0;                       % set initial velocity for particles
swarm(:,4,1) = 1000;                    % set the best value so far
plotObjFcn = 1;                         % set to zero if you do not need a final plot
tic;
%% The main loop of PSO
for iter = 1:maxIter
    swarm(:, 1, 1) = swarm(:, 1, 1) + swarm(:, 2, 1);       %update x position with the velocity
    swarm(:, 1, 2) = swarm(:, 1, 2) + swarm(:, 2, 2);       %update y position with the velocity
    swarm(:, 1, 3) = swarm(:, 1, 3) + swarm(:, 2, 3);       %update y position with the velocity
    swarm(:, 1, 4) = swarm(:, 1, 2) + swarm(:, 2, 4);       %update y position with the velocity
    swarm(:, 1, 5) = swarm(:, 1, 2) + swarm(:, 2, 5);       %update y position with the velocity   
    x_1 = swarm(:, 1, 1);                                       % get the updated position
    x_2 = swarm(:, 1, 2);                                       % updated position
    x_3 = swarm(:, 1, 3);                                       % updated position
    x_4 = swarm(:, 1, 4);                                       % updated position
    x_5 = swarm(:, 1, 5);                                       % updated position
    fval = objfcn([x_1 x_2 x_3 x_4 x_5]);                       % evaluate the function using the position of the particle
    
    % compare the function values to find the best ones
    for ii = 1:swarm_size
        if fval(ii,1) < swarm(ii,4,1)
            swarm(ii, 3, 1) = swarm(ii, 1, 1);                  % update best x position,
            swarm(ii, 3, 2) = swarm(ii, 1, 2);                  % update best y postions
            swarm(ii, 3, 3) = swarm(ii, 1, 3);                  % update best y postions
            swarm(ii, 3, 4) = swarm(ii, 1, 4);                  % update best y postions
            swarm(ii, 3, 5) = swarm(ii, 1, 5);                  % update best y postions
            swarm(ii, 4, 1) = fval(ii,1);                       % update the best value so far
        end
    end
    
    [~, gbest] = min(swarm(:, 4, 1));                           % find the best function value in total
    
    % update the velocity of the particles
    swarm(:, 2, 1) = k*(swarm(:, 2, 1) + correction_factor*(rand(swarm_size,1).*(swarm(:, 3, 1) ...
        - swarm(:, 1, 1))) + correction_factor*(rand(swarm_size,1).*(swarm(gbest, 3, 1) - swarm(:, 1, 1))));   %x1 velocity component
    swarm(:, 2, 2) = k*(swarm(:, 2, 2) + correction_factor*(rand(swarm_size,1).*(swarm(:, 3, 2) ...
        - swarm(:, 1, 2))) + correction_factor*(rand(swarm_size,1).*(swarm(gbest, 3, 2) - swarm(:, 1, 2))));   %x2 velocity component
    swarm(:, 2, 3) = k*(swarm(:, 2, 3) + correction_factor*(rand(swarm_size,1).*(swarm(:, 3, 3) ...
        - swarm(:, 1, 3))) + correction_factor*(rand(swarm_size,1).*(swarm(gbest, 3, 3) - swarm(:, 1, 3))));   %x3 velocity component
    swarm(:, 2, 4) = k*(swarm(:, 2, 4) + correction_factor*(rand(swarm_size,1).*(swarm(:, 3, 4) ...
        - swarm(:, 1, 3))) + correction_factor*(rand(swarm_size,1).*(swarm(gbest, 3, 4) - swarm(:, 1, 4))));   %x4 velocity component
    swarm(:, 2, 5) = k*(swarm(:, 2, 5) + correction_factor*(rand(swarm_size,1).*(swarm(:, 3, 5) ...
        - swarm(:, 1, 3))) + correction_factor*(rand(swarm_size,1).*(swarm(gbest, 3, 5) - swarm(:, 1, 5))));   %x5 velocity component
                                                    % un-comment this line to decrease the animation speed
    disp(['iteration: ' num2str(iter)]);
end
toc

[gbest,index] = min(swarm(:, 4, 1));
x1min=swarm(index, 3, 1);
x2min=swarm(index, 3, 2);
x3min=swarm(index, 3, 3);
x4min=swarm(index, 3, 4);
x5min=swarm(index, 3, 5);

disp(['Minimum = ' num2str(gbest) ' at (xi) = (',num2str(x1min),', ',num2str(x2min),', ',num2str(x3min),', ',num2str(x4min),', ',num2str(x5min),')']);

